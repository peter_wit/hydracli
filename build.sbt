name := "HydraCLI"

version := "1.0"

scalaVersion := "2.12.1"

libraryDependencies += "org.apache.httpcomponents" % "httpclient" % "4.5.3"
libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "1.0.6"
libraryDependencies += "org.json4s"   %% "json4s-jackson" % "3.5.2"


assemblyJarName in assembly := "hydracli.jar"
test in assembly := {}
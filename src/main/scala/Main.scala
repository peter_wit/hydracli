import command.CommandParser
import command.Command

import scala.util.control.Breaks._


/**
  * TODO: Remove the command
  */
object Main extends App{
  val exitCommands: List[String] = List("exit","quit","leave")
  var exit = false;
  println("Welcome to Hydra Command Line Interface")
  breakable { while(true){
    print(">")
    //readline and check exit
    val input = scala.io.StdIn.readLine()
    if(exitCommands.count(input.startsWith(_)) > 0)
      break
    try {
      CommandParser.parseCommand(input) match {
        case (None, None) => println("Does not match any available command")
        case (cmd, str) => {
          cmd.get.execute(str.get.split(" "))
        }
      }
    }catch {
      case e: Exception => "Exception occurred: Please do not be mean to the database and input reasonable options"
    }
  }}
}

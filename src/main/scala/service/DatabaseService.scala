package service

import org.apache.http._
import org.apache.http.client.methods.{HttpGet, HttpPost, HttpUriRequest}
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClientBuilder
import org.json4s.{DefaultFormats, Formats}
import model.Node
import scala.io.Source

object DatabaseService{
  implicit val jsonFormats: Formats = DefaultFormats
  def readDatabaseProperties(): Map[String,String] = {
    val db = Source.fromResource("Database.properties").getLines
    db.map(line => line.split("=")).map((arr) => arr(0) -> arr(1)).toMap
  }

  val dbInfo:Map[String,String] = readDatabaseProperties()
  var ip = "localhost"
  var port = 8090
  val url:String = s"http://$ip:$port"

  def setNodeInformation(node: Node): Unit = {
    val req = new HttpPost(url + "/set/information")
    req.setHeader("Content-type", "application/json")
    req.setEntity(new StringEntity(JsonService.makeNodeJson(node)))
    println(sendOutHttpRequest(req))
  }

  /**
    * CLUSTER
    */
  /**
    * CREATE THE CLUSTER MAKING THE CURRENT NODE THE MASTER AND ADD THE NODES INTO THE CLUSTER
    * Cluster related commands
    */
  def clusterCreate(nodes: List[Node]): Unit = {
    val req = new HttpPost(url + "/cluster/create")
    req.setHeader("Content-type", "application/json")
    req.setEntity(new StringEntity(JsonService.makeNodesJson(nodes)))
    println(sendOutHttpRequest(req))
  }

  /**
    * USE TO TELL THE SLAVE TO JOIN THE MASTER
    * require: master node
    * @param masternode
    */
  def clusterJoinSlave(masternode: Node): Unit = {
    val req = new HttpPost(url + "/cluster/join/slave")
    req.setHeader("Content-type", "application/json")
    req.setEntity(new StringEntity(JsonService.makeNodeJson(masternode)))
    println(sendOutHttpRequest(req))
  }

  /**
    * basically cluster add
    * THIS IS BASICALLY COMING FROM THE SLAVE TELLING THE MASTER THAT THE SLAVE IS READY
    * @param slavenode
    */
  def clusterJoinMaster(slavenode: Node): Unit = {
    val req = new HttpPost(url + "/cluster/join/master")
    req.setHeader("Content-type", "application/json")
    req.setEntity(new StringEntity(JsonService.makeNodeJson(slavenode)))
    println(sendOutHttpRequest(req))
  }





  /**
    * Database related commands
    */

  def databaseGeostrip(location1: String, location2: String, direction: String) = ???

  def databaseGeoradius(location: String, radius: String): Unit = {
    val geo = new HttpGet(url + "/database/georadius")
    geo.addHeader("location",location)
    geo.addHeader("radius",radius)
    println(JsonService.parseRecordsToString(sendOutHttpRequest(geo)))
  }

  def databaseGeobox(location1: String, location2: String): Unit = {
    val box = new HttpGet(url + "/database/geobox")
    box.addHeader("location1", location1)
    box.addHeader("location2",location2)
    println(JsonService.parseRecordsToString(sendOutHttpRequest(box)))
  }

  def databaseGet(key: String): Unit = {
    val get = new HttpGet(url + "/database/get")
    get.addHeader("location",key)
    println(JsonService.parseRecordToString(sendOutHttpRequest(get)))
  }

  /**
    * @param key
    * @param value
    */
  def databaseAdd(key: String, value: List[String]): Unit = {
    val post = new HttpPost(url + "/database/add")
    post.setHeader("Content-type", "application/json")
    post.setEntity(new StringEntity(JsonService.makeRecordJson(key,value)))
    println(sendOutHttpRequest(post))
  }

  def sendOutHttpRequest(req: HttpUriRequest): String = {
    val client = HttpClientBuilder.create().build()
    val response = client.execute(req)
    val tor = readResponse(response)
    client.close()
    tor
  }

  def readResponse(response: HttpResponse): String = {
    val responseStatusCode = response.getStatusLine.getStatusCode
    if (responseStatusCode >= 400 && responseStatusCode <= 499)
      "The database is currently unable to serve your request"
    else{
      scala.io.Source.fromInputStream(response.getEntity.getContent).mkString
    }
  }
}

package service

import java.io.File

import org.apache.http.conn.util.InetAddressUtils

object InputFilterService {

  def databaseAddCommandFilter(input: Seq[String]): Boolean = {
    input.length > 1 && checkIfValidCoordinate(input.head)
  }

  def databaseGetCommandFilter(input: Seq[String]): Boolean = {
    input.length == 1 && checkIfValidCoordinate(input.head)
  }

  /**
    * cluster create ...
    * @param input
    * @return
    */
  def clusterCreateCommandFilter(input: Seq[String]): Boolean = {
    if (input.length < 1) return false
    val ips = input.tail
    ips.count(checkIfValidNode(_)) == ips.length
  }

  def setNodeInformationCommandFilter(input: Seq[String]): Boolean = {
    input.length == 1 && (checkIfValidNode(input.head) || input.head.contains("localhost"))
  }

  /**
    * cluster join master slave
    * slave is in ip:port
    * @param input
    * @return
    */
  def clusterJoinMasterCommandFilter(input: Seq[String]): Boolean = {
    input.length == 1 && checkIfValidNode(input.head)
  }


  /**
    * cluster join slave master
    * master is in ip:port
    * literally the same as above
    * @param input
    * @return
    */
  def clusterJoinSlaveCommandFilter(input: Seq[String]): Boolean = {
    input.length == 1 && checkIfValidNode(input.head)
  }

  /**
    * database geobox location1 location
    * where locations are in double,double
    * @param input
    * @return
    */
  def databaseGeobox(input: Seq[String]): Boolean = {
    input.length == 2 && input.count(checkIfValidCoordinate(_)) == 2
  }

  /**
    * database georadius location radius
    * where
    * location is in double,double
    * radius is in double
    */
  def databaseGeoradius(input: Seq[String]): Boolean = {
    input.length == 2 && checkIfValidCoordinate(input.head) && checkIfStringIsDouble(input.tail.head)
  }

  def databaseSampleData(input: Seq[String]): Boolean = {
    input.length == 1 && checkIfFileExists(input.head)
  }


  /**
    * double,double
    * @param coord
    * @return
    */
  def checkIfValidCoordinate(coord: String): Boolean = {
    val arr = coord.split(",")
    if(arr.length != 2) return false
    checkIfStringIsDouble(arr(0)) && checkIfStringIsDouble(arr(1))
  }

  /**
    *
    * check if string is a node format -> (ip:port)
    * first part -> able to split into ip port  (ip:port)
    * second part -> ip is a valid IPv4Address
    * thrid part -> ensure that the port is a number and is a vaid port
    */
  def checkIfValidNode(ipt: String): Boolean = {
    val arr = ipt.split(":")
    if(arr.length != 2 || !InetAddressUtils.isIPv4Address(arr(0))) return false
    checkIfStringIsPort(arr(1))
  }

  def checkIfStringIsDouble(s: String): Boolean  = {
    try {
      Some(s.toDouble)
      true
    }
    catch {
      case e: Exception => false
    }
  }

  def checkIfStringIsPort(n: String): Boolean = {
    val intRegex = """(\d+)""".r
    n(1) match {
      case intRegex(str) => {
        if (!(str.toInt >= 1000 && str.toInt <= 65535)) return false
        true
      }
      case e => false
      case _ => false
    }
  }

  def checkIfFileExists(n: String): Boolean = {
    new File(n).exists()
  }

  def setCLICommandFilter(n: Seq[String]): Boolean = {
    n.length == 2 && checkIfStringIsPort(n.tail.head)
  }
}

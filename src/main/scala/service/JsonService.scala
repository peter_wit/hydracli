package service

import org.json4s.{DefaultFormats, Formats}
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._
import org.json4s.{DefaultFormats, Formats}
import org.json4s.jackson.JsonMethods.{compact, render}
import model.Node
object JsonService {
  implicit val jsonFormats: Formats = DefaultFormats

  def makeRecordJson(key: String, value: List[String]): String = {
    val json  = key -> value
    compact(render(json))
  }

  def makeNodesJson(nodes: List[Node]): String = {
    val json = "ips" -> nodes.map(node => ("ip" -> node.ip) ~ ("port" -> node.port))
    compact(render(json))
  }

  def makeNodeJson(node: Node): String = {
    val json = ("ip" -> node.ip) ~ ("port" -> node.port)
    compact(render(json))
  }

  def parseRecordsToString(records: String): String = {
    try{
      val jValue = parse(records)
      val recordString = jValue.extract[(String, List[(String,String)])]
      recordString._2.foldLeft("") {(ele,a) => ele + a._1 + " " + a._2 + "\n" }
    }catch {
      case e: Exception => "No such record found"
    }

  }

  def parseRecordToString(record: String): String = {
    try {
      val jValue = parse(record)
      val recordString = jValue.extract[(String, (String, String))]
      recordString._2._1 + " " + recordString._2._2
    } catch {
      case e: Exception => "No such record found"
    }
  }
}


package command

import service.{DatabaseService, InputFilterService}
import model.Node
class ClusterCreate extends Command {
  override def execute(input: Seq[String]): Unit = {
    if(!InputFilterService.clusterCreateCommandFilter(input)){
      println("Poor command signature; Proper command signature: cluster create ip:port ...")
      return
    }
    DatabaseService.clusterCreate(input.toList.map(n => n.split(":")).map(arr => Node(arr(0),arr(1))))
  }
}

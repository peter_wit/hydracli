package command

import model.Node
import service.{DatabaseService, InputFilterService}

class SetNodeInformation extends Command {
  override def execute(input: Seq[String]): Unit = {
    if(!InputFilterService.setNodeInformationCommandFilter(input)){
      println("Poor command signature; Proper command signature: set node information ip:port")
      return
    }
    DatabaseService.setNodeInformation(input.toList.map(n => n.split(":")).map(arr => Node(arr(0),arr(1))).head)
  }
}

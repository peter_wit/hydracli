package command

trait Command {
  def execute(input: Seq[String])
}

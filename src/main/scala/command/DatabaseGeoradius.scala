package command

import service.{DatabaseService, InputFilterService}

class DatabaseGeoradius extends Command {
  override def execute(input: Seq[String]): Unit = {
    if(!InputFilterService.databaseGeoradius(input)){
      println("Poor command signature; Proper command signature: cluster join slave master where slave is in ip:port")
      return
    }
    DatabaseService.databaseGeoradius(input.head,input.tail.head)
  }
}

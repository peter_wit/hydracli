package command

import service.{DatabaseService, InputFilterService}

class DatabaseSampleData extends Command {
  override def execute(input: Seq[String]): Unit = {
    if(!InputFilterService.databaseSampleData(input)){
      println("Poor command signature; File doesn't exist")
      return
    }
    val file = input.head
    val x = xml.XML.loadFile(file)

    (x \\ "root" \\ "row").foreach(x ⇒ {
      val timestamp: String = (System.currentTimeMillis() / 1000).toString
      val name: String = (x \\ "Name").text
      val latitude: String = (x \\ "Latitude").text
      val longitude: String = (x \\ "Longitude").text
      DatabaseService.databaseAdd(s"$latitude,$longitude", timestamp :: name :: Nil)
    })
  }
}

package command

import service.{DatabaseService, InputFilterService}

class DatabaseGet extends Command {
  override def execute(input: Seq[String]): Unit = {
    if(!InputFilterService.databaseGetCommandFilter(input)){
      println("Poor command signature; Proper command signature: get [key]")
      return
    }
    DatabaseService.databaseGet(input.head)
  }
}

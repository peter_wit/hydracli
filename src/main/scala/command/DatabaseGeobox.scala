package command

import model.Node
import service.{DatabaseService, InputFilterService}

class DatabaseGeobox extends Command {
  override def execute(input: Seq[String]): Unit = {
    if(!InputFilterService.databaseGeobox(input)){
      println("Poor command signature; Proper command signature: cluster join slave master where slave is in ip:port")
      return
    }
    DatabaseService.databaseGeobox(input.head,input.tail.head)
  }
}

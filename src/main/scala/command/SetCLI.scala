package command
import service._
class SetCLI extends Command {
  override def execute(input: Seq[String]): Unit = {
    if (InputFilterService.setCLICommandFilter(input)){
      println("Poor command signature; Proper command signature: set cli ip port")
    }
    DatabaseService.ip = input.head
    DatabaseService.port = input.tail.head.toInt
  }
}

package command

import model.Node
import service.{DatabaseService, InputFilterService}

class ClusterJoinSlave extends Command {
  override def execute(input: Seq[String]): Unit = {
    if(!InputFilterService.clusterJoinSlaveCommandFilter(input)){
      println("Poor command signature; Proper command signature: cluster join slave master where slave is in ip:port")
      return
    }
    DatabaseService.clusterJoinSlave(input.map(str => str.split(":")).map(arr => Node(arr(0),arr(1))).head)
  }
}

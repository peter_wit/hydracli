package command

class Help extends Command {
  override def execute(input: Seq[String]): Unit = {
    //Construct Help
    //Overview
    println("HydraCLI's Available commands")
    println("All commands can be in uppercase or lowercase or both.")
    println("Commands:")
    println("database add [key: latitude,longitude] [value ...] -> adding the value to the database and the value is reachable through the \'key\'")
    println("database get [key: latitude,longitude]         -> get the value from the database through the key")
    println("database georadius [coordinate: latitude,longitude] [radius: double]        -> get the values from the database through the key")
    println("database geobox [coordinate1: latitude,longitude] [coordinate2]        -> coordinate1 is the top left of the box, coordinate 2 is the bottom right of the box")
    println("cluster create [ip:port ...]  -> make the current node the master with the slaves at the given ip:port")
    println("cluster join slave [master ip:port]  -> make the current node a slave and join the master at the given ip:port")
    println("cluster join master [slave ip:port]  -> receive and grant the permission of the slave to be a part of the cluster")
    println("set node information [node ip:port]  -> set the current information of the node")
    println("set cli [ip] [port] -> set which database the cli should talk to")
    println("help              -> show help ")
  }
}

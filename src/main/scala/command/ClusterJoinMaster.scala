package command

import model.Node
import service.{DatabaseService, InputFilterService}

class ClusterJoinMaster extends Command {
  override def execute(input: Seq[String]): Unit = {
    if(!InputFilterService.clusterJoinMasterCommandFilter(input)){
      println("Poor command signature; Proper command signature: cluster join master slave where slave is in ip:port")
      return
    }
    DatabaseService.clusterJoinMaster(input.map(str => str.split(":")).map(arr => Node(arr(0),arr(1))).head)
  }
}

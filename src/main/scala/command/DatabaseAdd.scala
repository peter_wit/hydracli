package command

import service.{DatabaseService, InputFilterService}

/**
  * TO IMPLEMENT
  */
class DatabaseAdd extends Command{
  override def execute(input: Seq[String]): Unit = {
    if(!InputFilterService.databaseAddCommandFilter(input)){
      println("Poor command signature; Proper command signature: database add [key] [value]")
      return
    }
    DatabaseService.databaseAdd(input.head, input.tail.toList)
  }
}

package command

object CommandParser {
  val commandMap: Map[String,Command] = Map(
    "database add" -> new DatabaseAdd(),
    "help" -> new Help(),
    "database get" -> new DatabaseGet(),
    "cluster create" -> new ClusterCreate(),
    "cluster join master" -> new ClusterJoinMaster(),
    "cluster join slave" -> new ClusterJoinSlave(),
    "database geobox" -> new DatabaseGeobox(),
    "database georadius" -> new DatabaseGeoradius(),
    "database geostrip" -> new DatabaseGeostrip(),
    "set node information" -> new SetNodeInformation(),
    "database sampledata" → new DatabaseSampleData(),
    "set cli" -> new SetCLI
  )

  /**
    * TODO: Parse the damn thing
    * @param input
    * @return
    */
  def parseCommand(input: String): (Option[Command], Option[String]) = {
    val cmd = commandMap.keySet.filter(input.toLowerCase.startsWith(_)).toList
    if(cmd.nonEmpty)  (Option(commandMap(cmd.head)), Option(input.substring(cmd.head.length).trim))
    else  (None, None)

  }
}
